use regex::Regex;
use std::fmt::{Display, Formatter};
use std::time::{Duration, Instant};

use crate::ringtone;
use errr::{raise, Result};
use sui::Tui;

const HOUR: &str = "hour";
const MINUTE: &str = "minute";
const SECOND: &str = "second";
const TIMER_FORMAT: &str = r"((?P<hour>\d+)h)?((?P<minute>\d+)m)?((?P<second>\d+)s)?";

#[derive(Debug)]
pub struct Timer {
    pub deadline: Duration,
    pub msg: Option<String>,
}

impl Timer {
    pub fn new(dur: &str, msg: Option<String>) -> Result<Self> {
        Ok(Timer {
            deadline: parse_duration(dur)?,
            msg,
        })
    }

    pub fn run(&self) -> Result<()> {
        let start = Instant::now();
        let tui = Tui::default();
        while !tui.is_finished() {
            let elapsed = start.elapsed();
            if self.deadline <= elapsed {
                ringtone::addams_family_theme();
                break;
            }

            let remain: Duration = self.deadline - elapsed;
            tui.draw_text(&self.to_fmt(remain.as_secs()))?;
        }
        Ok(())
    }

    pub fn msg(mut self, txt: Option<String>) -> Self {
        self.msg = txt;
        self
    }

    fn to_fmt(&self, remain: u64) -> String {
        let mut fmt: String = remain_to_fmt(remain);
        if let Some(m) = &self.msg {
            fmt = m.to_lowercase() + " " + &fmt;
        }
        fmt
    }
}

impl From<Duration> for Timer {
    fn from(value: Duration) -> Self {
        Timer {
            deadline: value,
            msg: None,
        }
    }
}

pub fn parse_duration(duration: &str) -> Result<Duration> {
    let caps = Regex::new(TIMER_FORMAT)?
        .captures(duration)
        .ok_or(Box::new(TimerError::WrongTimeFormat))?;
    let h: u64 = caps.name(HOUR).map_or(0, |m| m.as_str().parse().unwrap());
    let m: u64 = caps.name(MINUTE).map_or(0, |m| m.as_str().parse().unwrap());
    let s: u64 = caps.name(SECOND).map_or(0, |m| m.as_str().parse().unwrap());
    if h == 0 && m == 0 && s == 0 {
        raise!(TimerError::WrongTimeFormat);
    }
    Ok(Duration::new(3600 * h + 60 * m + s, 0))
}

fn remain_to_fmt(remain: u64) -> String {
    let (hours, minutes, seconds) = (remain / 3600, (remain % 3600) / 60, remain % 60);
    if hours == 0 {
        format!("{minutes:02}:{seconds:02}")
    } else {
        format!("{hours:02}:{minutes:02}:{seconds:02}")
    }
}

#[derive(Debug)]
pub enum TimerError {
    WrongTimeFormat,
}

impl std::error::Error for TimerError {}
impl Display for TimerError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            TimerError::WrongTimeFormat => write!(f, "Wrong input time format"),
        }
    }
}
