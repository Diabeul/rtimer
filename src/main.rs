use clap::Parser;
use std::process::exit;

mod args;

use args::TimerArgs;

fn main() {
    let args: TimerArgs = TimerArgs::parse();
    match rtimer::run_from(args.time, args.msg) {
        Ok(_) => exit(0),
        Err(e) => {
            eprintln!("Oh noes, we cannot create the timer :(\n {e}");
            exit(2)
        }
    }
}
