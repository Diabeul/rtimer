use clap::Parser;
use rtimer::parse_duration;
use std::fmt::Error;
use std::time::Duration;

#[derive(Debug, Parser)]
#[clap(author, version, about)]
pub struct TimerArgs {
    /// the duration of the timer in format: XhXmXs, ex: 25s, 1m50s, 2h45m50s
    #[arg(value_parser = parse)]
    pub time: Duration,

    /// Optional message for the timer
    #[arg(short, long)]
    pub msg: Option<String>,
}

fn parse(duration: &str) -> Result<Duration, Error> {
    parse_duration(duration).map_err(|_| Error)
}
