extern crate core;

mod ringtone;
mod timer;

use std::time::Duration;

use errr::Result;
use timer::Timer;

pub use timer::parse_duration;

pub fn run(dur: &str, msg: Option<String>) -> Result<()> {
    Timer::new(dur, msg)?.run()?;
    Ok(())
}

pub fn run_from(dur: Duration, msg: Option<String>) -> Result<()> {
    Timer::from(dur).msg(msg).run()?;
    Ok(())
}
