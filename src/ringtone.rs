use phf::phf_map;
use std::process::Command;
use std::thread;
use std::time::Duration;

static KEYNOTES: phf::Map<&'static str, f32> = phf_map! {
  "c" => 261.63,
  "c#" => 277.18,
  "d" => 293.66,
  "d#" => 311.13,
  "e" => 329.63,
  "f" => 349.23,
  "f#" => 369.99,
  "g" => 392.00,
  "g#" => 415.30,
  "a" => 440.00,
  "a#" => 466.16,
  "b" => 493.88,
  "C" => 523.88,
  "C#" => 554.37,
  "D" => 587.33,
};

fn play_ringtone(duration: f32, freq: f32) {
    Command::new("play")
        .arg("-nq")
        .arg("-t")
        .arg("alsa")
        .arg("synth")
        .arg(duration.to_string())
        .arg("sine")
        .arg(freq.to_string())
        .status()
        .unwrap();
}

fn play_chords(chords: &[&str]) {
    for a in chords {
        play_ringtone(0.1, KEYNOTES[a])
    }
}

pub fn addams_family_theme() {
    let short = Duration::from_millis(300);
    let long = Duration::from_millis(900);
    play_chords(&["f", "g", "a", "a#"]);
    thread::sleep(long);
    play_chords(&["g", "a", "a#", "C"]);
    thread::sleep(long);
    play_chords(&["g", "a", "a#", "C"]);
    thread::sleep(short);
    play_chords(&["g", "a", "a#", "C"]);
    thread::sleep(short);
    play_chords(&["f", "g", "a", "a#"]);
    thread::sleep(long);
}
