# Rust Timer

```
Usage: rtimer <TIME>

Arguments:
  <TIME>  the duration of the timer in format: XhXmXs, ex: 25s, 1m50s, 2h45m50s

Options:
  -h, --help     Print help
  -V, --version  Print version

```
